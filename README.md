
# Termux Scripts

This repository contains various scripts used for automating things on my
Android phone.

## Contents

 * Tasker scripts
   * `start_ssh.sh` - starts an OpenSSH server
   * `stop_ssh.sh` - stops the SSH server

